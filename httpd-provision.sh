#!/bin/bash

if (( $# > 0 ))
then
    hostname=$1
else
    echo "Please supply a hostname or IP address as an argument" 1>&2
    exit 1
fi

scp -o StrictHostKeyChecking=no -i ~/.ssh/BrianTingKey.pem httpd.init instructor@$hostname:httpd.init 

ssh -o StrictHostKeyChecking=no instructor@$hostname '
sudo yum -y install httpd
if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
    sudo systemctl start httpd
else
    exit 1
fi

sudo mv ~/httpd.init /etc/init.d/webpage
sudo chmod +x /etc/init.d/webpage
sudo chkconfig --add webpage
sudo systemctl enable webpage
sudo /etc/init.d/webpage start
'
