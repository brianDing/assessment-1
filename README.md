# Assessment 1 

## Setting up machines
- chmod +x httpd-provision.sh ubuntu-provision.sh
- 

## Requirements
- 3 servers
  - 1x AWS Linux serving a page
  - 1x Ubuntu VM serving a different page
  - 1x Load balancer with HAProxy
- 2 servers that are not the load balancer
  - User called instructor with a password of m4g1cAut0L0t10n
  - sshd to allow password login + firewall allow anywhere to ssh 
- script called user_data to provision the machines
  


## Steps 
- Setting up users
  - Has a user called instructor with a password called m4g1cAut0L0t10n (those are 1s and 0s).
    - sudo useradd -m instructor
    - sudo passwd instructor
    - using ec2-user sudo into sshd_config to enable password authentication 
    - sudo systemctl reload ssh
    - sudo su to become root user
    - edit /etc/sudoers.d/90-cloud-init-users with nano
    - add in line: instructor ALL=(ALL) NOPASSWD:ALL for no password sudo access


- Webserver EC2
  - install webserver 
  - Write to index.html and place in root directory for webserver 


- Load balancer
  - Install loadbalancer HAProxy
  - Move in config file 