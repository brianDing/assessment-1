#!/bin/bash

if (( $# > 0 ))
then
    hostname=$1
else
    echo "Please supply a hostname or IP address as an argument" 1>&2
    exit 1
fi

scp -o StrictHostKeyChecking=no -i ~/.ssh/BrianTingKey.pem ubuntu.init instructor@$hostname:ubuntu.init 

ssh -o StrictHostKeyChecking=no instructor@$hostname '
sudo apt update
sudo apt intsall -y nginx
sudo ufw allow 'Nginx HTTP'
sudo mv ~/ubuntu.init /etc/init.d/webpage
sudo chmod +x /etc/init.d/webpage
sudo update-rc.d webpage defaults
sudo service webpage start
'
