#!/bin/bash

if (( $# > 1 ))
then
    httpd=$1
    ubuntu=$2
else
    echo "Please supply one hostname or IP address for a AWS Linux system as well as one for a Ubuntu system as an argument" 1>&2
    exit 1
fi


